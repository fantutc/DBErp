<?php
/**
 * DBERP 进销存系统
 *
 * ==========================================================================
 * @link      http://www.dberp.net/
 * @copyright 北京珑大钜商科技有限公司，并保留所有权利。
 * @license   http://www.dberp.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风 <baron@loongdom.cn>
 *
 */

namespace Customer\Repository;

use Customer\Entity\CustomerCategory;
use Doctrine\ORM\EntityRepository;

class CustomerCategoryRepository extends EntityRepository
{
    /**
     * 获取最大客户分类id
     * @return float|int|mixed|string
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getMaxCustomerCategoryId()
    {
        $query = $this->getEntityManager()->createQueryBuilder();
        $query->select('MAX(c.customerCategoryId) as maxCustomerCategoryId')->from(CustomerCategory::class, 'c');

        $maxId = $query->getQuery()->getSingleScalarResult();

        return $maxId == null ? 1 : $maxId + 1;
    }
}