<?php
/**
 * DBERP 进销存系统
 *
 * ==========================================================================
 * @link      http://www.dberp.net/
 * @copyright 北京珑大钜商科技有限公司，并保留所有权利。
 * @license   http://www.dberp.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风 <baron@loongdom.cn>
 *
 */

namespace Report\Form;

use Laminas\Form\Form;
use Laminas\I18n\Translator\Translator;

class SearchStockForm extends Form
{
    private $translator;

    public function __construct($name = 'search-stock-form', array $options = [])
    {
        parent::__construct($name, $options);

        $this->setAttribute('method', 'get');
        $this->setAttribute('class', 'form-horizontal');

        $this->translator = new Translator();

        $this->addElements();
        $this->addInputFilter();
    }

    public function addElements()
    {
        $this->add([
            'type'  => 'text',
            'name'  => 'goods_name',
            'attributes'    => [
                'id'            => 'goods_name',
                'class'         => 'form-control input-sm',
                'placeholder'   => $this->translator->translate('商品名称')
            ]
        ]);

        $this->add([
            'type'  => 'text',
            'name'  => 'goods_number',
            'attributes'    => [
                'id'            => 'goods_number',
                'class'         => 'form-control input-sm',
                'placeholder'   => $this->translator->translate('商品编号')
            ]
        ]);

        $this->add([
            'type'  => 'text',
            'name'  => 'goods_spec',
            'attributes'    => [
                'id'            => 'goods_spec',
                'class'         => 'form-control input-sm',
                'placeholder'   => $this->translator->translate('商品规格')
            ]
        ]);

        $this->add([
            'type'  => 'text',
            'name'  => 'start_stock',
            'attributes'    => [
                'id'            => 'start_stock',
                'class'         => 'form-control input-sm',
                'placeholder'   => $this->translator->translate('起始库存')
            ]
        ]);

        $this->add([
            'type'  => 'text',
            'name'  => 'end_stock',
            'attributes'    => [
                'id'            => 'end_stock',
                'class'         => 'form-control input-sm',
                'placeholder'   => $this->translator->translate('结束库存')
            ]
        ]);

        $this->add([
            'type'  => 'select',
            'name'  => 'warehouse_id',
            'attributes'    => [
                'id'            => 'warehouse_id',
                'class'         => 'form-control input-sm'
            ]
        ]);
    }

    public function addInputFilter()
    {
        $inputFilter = $this->getInputFilter();

        $inputFilter->add([
            'name'      => 'goods_name',
            'required'  => false,
            'filters'   => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ]
        ]);

        $inputFilter->add([
            'name'      => 'goods_number',
            'required'  => false,
            'filters'   => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ]
        ]);

        $inputFilter->add([
            'name'      => 'goods_spec',
            'required'  => false,
            'filters'   => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ]
        ]);

        $inputFilter->add([
            'name'      => 'start_stock',
            'required'  => false,
            'filters'   => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags'],
                ['name' => 'ToFloat']
            ]
        ]);

        $inputFilter->add([
            'name'      => 'end_stock',
            'required'  => false,
            'filters'   => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags'],
                ['name' => 'ToFloat']
            ]
        ]);

        $inputFilter->add([
            'name'      => 'warehouse_id',
            'required'  => false,
            'filters'   => [
                ['name' => 'ToInt']
            ]
        ]);
    }
}