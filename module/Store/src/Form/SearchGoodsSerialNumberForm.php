<?php
/**
 * DBERP 进销存系统
 *
 * ==========================================================================
 * @link      http://www.dberp.net/
 * @copyright 北京珑大钜商科技有限公司，并保留所有权利。
 * @license   http://www.dberp.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风 <baron@loongdom.cn>
 *
 */

namespace Store\Form;

use Laminas\Form\Form;
use Laminas\I18n\Translator\Translator;

class SearchGoodsSerialNumberForm extends Form
{
    private $translator;

    public function __construct($name = 'search-goods-serial-number-form', array $options = [])
    {
        parent::__construct($name, $options);

        $this->setAttribute('method', 'get');
        $this->setAttribute('class', 'form-horizontal');

        $this->translator = new Translator();

        $this->addElements();
        $this->addInputFilter();
    }

    protected function addElements()
    {
        $this->add([
            'type'  => 'text',
            'name'  => 'serial_number',
            'attributes'    => [
                'id'            => 'serial_number',
                'class'         => 'form-control input-sm',
                'placeholder'   => $this->translator->translate('序列号')
            ]
        ]);

        $this->add([
            'type'  => 'text',
            'name'  => 'goods_name',
            'attributes'    => [
                'id'            => 'goods_name',
                'class'         => 'form-control input-sm',
                'placeholder'   => $this->translator->translate('商品名称')
            ]
        ]);

        $this->add([
            'type'  => 'text',
            'name'  => 'warehouse_name',
            'attributes'    => [
                'id'            => 'warehouse_name',
                'class'         => 'form-control input-sm',
                'placeholder'   => $this->translator->translate('仓库名称')
            ]
        ]);

        $this->add([
            'type'  => 'select',
            'name'  => 'serial_number_state',
            'attributes'    => [
                'id'            => 'serial_number_state',
                'class'         => 'form-control input-sm'
            ]
        ]);

        $this->add([
            'type'  => 'text',
            'name'  => 'o_start_time',
            'attributes'    => [
                'id'            => 'o_start_time',
                'class'         => 'form-control input-sm',
                'placeholder'   => $this->translator->translate('开始时间')
            ]
        ]);
        $this->add([
            'type'  => 'text',
            'name'  => 'o_end_time',
            'attributes'    => [
                'id'            => 'o_end_time',
                'class'         => 'form-control input-sm',
                'placeholder'   => $this->translator->translate('结束时间')
            ]
        ]);

        $this->add([
            'type'  => 'text',
            'name'  => 'i_start_time',
            'attributes'    => [
                'id'            => 'i_start_time',
                'class'         => 'form-control input-sm',
                'placeholder'   => $this->translator->translate('开始时间')
            ]
        ]);
        $this->add([
            'type'  => 'text',
            'name'  => 'i_end_time',
            'attributes'    => [
                'id'            => 'i_end_time',
                'class'         => 'form-control input-sm',
                'placeholder'   => $this->translator->translate('结束时间')
            ]
        ]);
    }

    protected function addInputFilter()
    {
        $inputFilter = $this->getInputFilter();

        $inputFilter->add([
            'name'      => 'serial_number',
            'required'  => false,
            'filters'   => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags'],
                ['name' => 'HtmlEntities']
            ]
        ]);

        $inputFilter->add([
            'name'      => 'goods_name',
            'required'  => false,
            'filters'   => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ]
        ]);

        $inputFilter->add([
            'name'      => 'warehouse_name',
            'required'  => false,
            'filters'   => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ]
        ]);

        $inputFilter->add([
            'name'      => 'serial_number_state',
            'required'  => false,
            'filters'   => [
                ['name' => 'ToInt']
            ]
        ]);

        $inputFilter->add([
            'name'      => 'o_start_time',
            'required'  => false,
            'filters'   => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ]
        ]);

        $inputFilter->add([
            'name'      => 'o_end_time',
            'required'  => false,
            'filters'   => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ]
        ]);

        $inputFilter->add([
            'name'      => 'i_start_time',
            'required'  => false,
            'filters'   => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ]
        ]);

        $inputFilter->add([
            'name'      => 'i_end_time',
            'required'  => false,
            'filters'   => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ]
        ]);
    }
}