DROP TABLE IF EXISTS `dberp_goods_custom`;
CREATE TABLE IF NOT EXISTS `dberp_goods_custom` (
  `custom_id` int(11) NOT NULL AUTO_INCREMENT,
  `goods_id` int(11) NOT NULL,
  `custom_title` varchar(50) NOT NULL,
  `custom_content` varchar(200) NOT NULL,
  `custom_key` int(2) NOT NULL,
  PRIMARY KEY (`custom_id`),
  KEY `goods_id` (`goods_id`),
  KEY `custom_key` (`custom_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='商品自定义表';