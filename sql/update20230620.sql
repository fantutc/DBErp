ALTER TABLE `dberp_goods` ADD `goods_serial_number_state` TINYINT(1) NOT NULL DEFAULT '0' AFTER `goods_price`;
DROP TABLE IF EXISTS `dberp_goods_serial_number`;
CREATE TABLE IF NOT EXISTS `dberp_goods_serial_number` (
    `number_id` int(11) NOT NULL AUTO_INCREMENT,
    `serial_number` varchar(150) NOT NULL,
    `serial_number_state` tinyint(1) NOT NULL DEFAULT '0',
    `goods_id` int(11) NOT NULL,
    `warehouse_id` int(11) NOT NULL DEFAULT '0',
    `serial_number_type` tinyint(1) NOT NULL,
    `outbound_in_id` int(11) NOT NULL,
    `outbound_time` int(10) DEFAULT '0',
    `in_time` int(10) DEFAULT '0',
    `return_type` tinyint(1) DEFAULT '0',
    `return_time` int(10) DEFAULT '0',
    `add_time` int(10) NOT NULL,
    PRIMARY KEY (`number_id`),
    KEY `serial_number` (`serial_number`),
    KEY `serial_number_state` (`serial_number_state`),
    KEY `goods_id` (`goods_id`),
    KEY `add_time` (`add_time`),
    KEY `serial_number_type` (`serial_number_type`),
    KEY `warehouse_id` (`warehouse_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='商品序列号表';
ALTER TABLE `dberp_sales_order_goods` ADD `goods_serial_number_state` TINYINT(1) NOT NULL DEFAULT '0' AFTER `sales_goods_amount`;
ALTER TABLE `dberp_sales_order_goods_return` ADD `goods_serial_number_str` TEXT NULL AFTER `goods_return_num`;
ALTER TABLE `dberp_sales_send_warehouse_goods` ADD `goods_serial_number_str` TEXT NULL AFTER `sales_order_id`;
ALTER TABLE `dberp_purchase_order_goods` ADD `goods_serial_number_str` TEXT NULL AFTER `p_goods_info`, ADD `goods_serial_number_state` TINYINT(1) NOT NULL DEFAULT '0' AFTER `goods_serial_number_str`;
ALTER TABLE `dberp_purchase_order_goods_return` ADD `goods_serial_number_str` TEXT NULL AFTER `goods_return_num`;
ALTER TABLE `dberp_purchase_warehouse_order_goods` ADD `goods_serial_number_str` TEXT NULL AFTER `goods_unit`;
ALTER TABLE `dberp_ex_warehouse_order_goods` ADD `goods_serial_number_str` TEXT NULL AFTER `goods_unit`;
ALTER TABLE `dberp_other_warehouse_order_goods` ADD `goods_serial_number_str` TEXT NULL AFTER `goods_unit`;
ALTER TABLE `dberp_stock_transfer_goods` ADD `goods_serial_number_str` TEXT NULL AFTER `goods_unit`;